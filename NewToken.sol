pragma solidity ^0.8.12;

import "./ERC20Standard.sol";

contract NewToken is ERC20Standard
 {
	constructor()
	 public 
	{
		totalSupply = 1000;
		name = "Accessory";
		decimals = 4;
		symbol = "ACR";
		version = "1.0";
		balances[msg.sender] = totalSupply;
	}
}